// DOM elements
const elCheckingsAccount = document.getElementById('checkingAccount');
const elBtnApplyForLoan = document.getElementById('btnApplyForLoan');
const elPayrollAccount = document.getElementById('payrollAccount');
const elBtnTransferMoneyToBank = document.getElementById('btnTransferMoneyToBank')
const elBtnStartWorking = document.getElementById('btnStartWorking');
const elLaptopSelection = document.getElementById('laptopSelection');
const elLaptopFeatures = document.getElementById('laptopFeatures');
const elLaptopImage = document.getElementById('laptopPicture')
const elLaptopName = document.getElementById('laptopModel');
const elLaptopCPU = document.getElementById('laptopCPU');
const elLaptopScreenSize = document.getElementById('laptopScreenSize');
const elLaptopHarddrive = document.getElementById('laptopHarddrive');
const elLaptopDescription = document.getElementById('laptopDescription');
const elLaptopPrice = document.getElementById('laptopPrice');
const elBtnBuyLaptop = document.getElementById('btnBuyLaptop');

let previousLoaner = false;
let checkingAccount = 100;
let payrollAccount = 100;

elBtnApplyForLoan.addEventListener('click', function(event) {
    applyForLoan();
    updateUI();
})

function applyForLoan() {
    let amountToLoan = showInputPrompt();
    if(amountToLoan > checkingAccount*2) {
        showAlert("The amount you want to borrow is too high. Try a lower figure.");
    } else if (previousLoaner){
        showAlert("You have previously borrowed money from us. You are not eligable for a second one.");
    } else if (amountToLoan <= checkingAccount*2 && !previousLoaner) {
        checkingAccount += amountToLoan;
        previousLoaner = true;
        showAlert(`You have borrowed the amount of Kr. ${amountToLoan},-. Your new balance is 
        Kr. ${checkingAccount},-`);
    }
}

function showInputPrompt() {
    let amountToLoan = prompt("Please input the amount you want to borrow.");
    if(amountToLoan != null) {
        return amountToLoan = parseInt(amountToLoan);
    }
 }

 function showAlert(alert) {
    return window.alert(alert);
 }

elBtnTransferMoneyToBank.addEventListener('click', function(event) {
    transferPayrollToAccount();
    updateUI();
})

function transferPayrollToAccount() {
    if(payrollAccount > 0) {
        checkingAccount += payrollAccount;
        payrollAccount = 0;
    }
}

elBtnStartWorking.addEventListener('click', function() {
    addMoneyToPayrollAccount();
    updateUI();
})

const addMoneyToPayrollAccount = () => payrollAccount += 100;

const laptopsForSale = [
    { 
        name: "HP Elitebook", screensize: "17\"", cpu: "AMD 3900X", harddrive: "512GB", price: 5999, 
        id: "elitebook", description: "A descent notebook designed for the daily tasks such as web, mail and watching Netflix.",
        image: "https://images-na.ssl-images-amazon.com/images/I/41VVKm9x8TL._AC_.jpg"},
    { 
        name: "Asus Zenbook Pro Duo", screensize: "17\"", cpu: "AMD 3700X", harddrive: "1028GB", price: 24999, 
        id: "zenbook", description: "Dual-screen laptop with power to do pretty much anything you want.",
        image: "https://www.komplett.no/img/p/1080/1156007_3.jpg"},
    { 
        name: "Microsoft Surface", screensize: "15\"", cpu: "i7 6700k", harddrive: "512GB", price: 12999,
        id: "surface", description: "A 2-in-1 laptop with powerfull specs. Good for school work.",
        image: "https://www.ednasia.com/wp-content/uploads/sites/3/2020/04/contenteetimes-images-edn-brians-brain-surface-pro-laptop-tablet.jpg"},
    { 
        name: "MacBook Pro", screensize: "15\"", cpu: "i3 5400", harddrive: "512GB", price: 21999,
        id: "macbook", description: "An overpriced, good looking computer. Good for school work", 
        image: "https://austinmacworks.com/wp-content/uploads/2019/06/new-MacBook-Pro-video-photo-1080x675.jpg"}
];

elLaptopSelection.addEventListener('click', function() {
    updateUI();
})

let laptop = {};

let selectedLaptop = elLaptopSelection;

showSelectedLaptop();

function getSelectedLaptop() {
    for(let i = 0; i < laptopsForSale.length; i++) {
        if(selectedLaptop.value === laptopsForSale[i].id) {
            laptop = laptopsForSale[i];
            return laptop;
        }
    }
}

function showSelectedLaptop() {
    let laptop = getSelectedLaptop();

    elLaptopName.innerText = laptop.name;
    elLaptopCPU.innerText = laptop.cpu;
    elLaptopScreenSize.innerText = laptop.screensize;
    elLaptopHarddrive.innerText = laptop.harddrive;
    elLaptopDescription.innerText = laptop.description;
    elLaptopPrice.innerText = laptop.price;
    elLaptopFeatures.innerText = laptop.description;
    elLaptopImage.src = laptop.image;
}

function checkIfEligableForPurchace() {
    if(checkingAccount >= laptop.price) {
        return true;
    }
    return false;
}

function purchaseLaptop() {
    if(checkIfEligableForPurchace()) {
        checkingAccount -= laptop.price;
        window.alert(`You just purchased ${laptop.name}. Your new balance 
        is now Kr. ${checkingAccount},-`);
        previousLoaner = false;
    } else {
        window.alert(`You do not have enough funds to purchase this laptop. Work harder
        or try to apply for a loan!`);
    }
}

elBtnBuyLaptop.addEventListener('click', function(){
    purchaseLaptop();
    updateUI();
})

function updateUI() {
    elCheckingsAccount.innerText = checkingAccount;
    elPayrollAccount.innerText = payrollAccount;

    showSelectedLaptop();
}